var $storyTitle;

$(document).on("click", "#storyName", function () {
    var currElmModelAttr = $(this).attr('data-model-attr');
    $this = $(this);
    var input = $('<input />', {
        'type': 'text',
            'name': currElmModelAttr,
            'class': 'storyEditable',
            'value': $(this).text()
    });
    $(this).replaceWith(input);
    input.datepicker({
        onSelect: function (date) {
            $this.text(date);
            input.replaceWith($this);
            // input.blur();
        }
    }).focus()
    $(document).on("blur change", "input", function () {
        setTimeout(function () {
            var value = input.val();

            $this.text(value);
            input.replaceWith($this);
        }, 100);
    });
});