koa = require 'koa'
Router = require 'koa-joi-router'
serve = require 'koa-static'
path = require 'path'
jade = require 'koa-jade'
bodyParser = require 'koa-bodyparser'
flow = require './flow'
app = koa()

router = Router()

app.use serve(path.join(__dirname, '..', '/public'))

app.use jade.middleware({
  viewPath: path.join(__dirname, '..', '/views')
})

app.use bodyParser()

app.use router.middleware()

app.use (next) ->
  start = new Date
  yield next
  ms = new Date - start
  console.log '%s %s - %s', this.method, this.url, ms

app.use (next) ->
  story = yield flow.getStory(@)
  if story?
    @render 'index.jade', story.get()
  else
    @render 'newStory.jade', @request.query

router.route {
  method: 'post'
  path: '/createStory'
  handler: (next) ->
    saved = yield flow.saveStory(@request.body)
    this.redirect '/?story=' + saved.get().hash
}

router.route {
  method: 'post'
  path: '/addChapter'
  handler: (next) ->
    data = @request.body
    hash = data.hash
    saved = yield flow.addChapter data
    this.redirect '/?story=' + hash + "#" + saved.get().id
}

module.exports = exports = app