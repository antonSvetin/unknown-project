app = require './bootstrap'
db = require './database'

db.sync().then () ->
  app.listen 1234, ->
    console.log "App running on 1234 port!"