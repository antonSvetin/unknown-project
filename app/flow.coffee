Promise = require 'bluebird'
crypto = require 'crypto'

exports.getStory = (ctx) ->
  Promise.bind @
  .then () ->
    hash = ctx.request.query?.story
    DB.models.Story.findOne {where:{hash: hash}, include:[{as:'Chapters', model:DB.models.Chapter}]}

exports.saveStory = (data) ->
  Promise.bind @
  .then () ->
    if data
      current_date = (new Date()).valueOf().toString();
      random = Math.random().toString()
      data.hash = crypto.createHash('sha1').update(current_date + random).digest('hex')
      DB.models.Story.create data
      .then (data) ->
        return data
    else
      return null

exports.addChapter = (data) ->
  Promise.bind @
  .then () ->
    if data?.StoryId
      DB.models.Story.findOne {where:{id: data.StoryId}}
      .then (story) ->
        delete data.hash
        delete data.StoryId
        DB.models.Chapter.build data
        .save().then (saved) ->
          story.addChapters([saved])