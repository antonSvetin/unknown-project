Sequelize = require 'sequelize'
db = new Sequelize 'app_xyz', 'xyz', 'wuvdfaHGFGUQcsdj7XsV', {
  host: 'localhost'
  dialect: 'mysql'
  pool:
    max: 10
    min: 0
    idle: 10000
  logging: false
}

options = {
  paranoid: true
  timestamps: true
}

Story = db.define 'Story', {
  title: Sequelize.STRING
  hash: Sequelize.STRING
}, options

Chapter = db.define 'Chapter', {
  text: Sequelize.TEXT
  photo: Sequelize.STRING
}, options

Story.hasMany(Chapter, {as:'Chapters'})
Chapter.belongsTo(Story, {as:'Story'})


GLOBAL.DB =
  seq: db
  Sequelize: Sequelize
  models:
    Story: Story
    Chapter: Chapter

module.exports = exports = db
