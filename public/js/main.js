var $storyTitle;

$(document).on("click", "#storyName", function () {
    var currElmModelAttr = $(this).attr('data-model-attr');
    $this = $(this);
    var input = $('<input />', {
        'type': 'text',
            'name': currElmModelAttr,
            'class': 'storyEditable',
            'value': $(this).text()
    });
    $(this).replaceWith(input);
    input.datepicker({
        onSelect: function (date) {
            $this.text(date);
            input.replaceWith($this);
            // input.blur();
        }
    }).focus()
    $(document).on("blur change", "input", function () {
        setTimeout(function () {
            var value = input.val();

            $this.text(value);
            input.replaceWith($this);
        }, 100);
    });
});

$("#storyTxt").on("blur change", function() {
    $("#submitChapter").prop('disabled', true);
    var q = this.value;
    results = 1;
    //if (location.search) q = unescape(location.search);
    $.getScript('https://ajax.googleapis.com/ajax/services/search/images?callback=processResults&rsz=' + results +
      '&v=1.0&q=' + escape(q));
});

$("#newPhoto").on("change", function() {
    $("#submitChapter").prop('disabled', true);
    var path = this.value;
    $("#imgPhoto").attr('src', path);
});

$("#imgPhoto").on("error", function() {
    $(this).hide();
});

obj = $("#newChapter .rndImgBox");
img = $('<img style="display:none;" id="imgPhoto" src="" />');

obj.append(img);

img.load( function(){
    $("#submitChapter").prop('disabled', false);
    img.show();
    $("#newChapter .rndImgBox span").hide();
}).error( function(){
    img.hide();
    if(img.attr("src") != ""){
        img.hide();
        $("#newChapter .rndImgBox span").show();
    }else{
        $("#newChapter .rndImgBox span").hide();
        $("#submitChapter").prop('disabled', false);
    }
});

function processResults(data) {
  $.each(data.responseData.results, function (i, data) {
    $("#imgPhoto").attr('src', 'https://encrypted-tbn1.gstatic.com/images?q=tbn:' + data.imageId);
    $("#newPhoto").val('https://encrypted-tbn1.gstatic.com/images?q=tbn:' + data.imageId);
  });

}
